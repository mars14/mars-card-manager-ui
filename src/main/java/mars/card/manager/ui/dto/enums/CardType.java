package mars.card.manager.ui.dto.enums;

public enum CardType {
    CORPORATION, BLUE, GREEN, RED
}
