package mars.card.manager.ui.dto.enums;

public enum FieldType {
    GROUND, OCEAN, VOLCANO, NOCTIS_CITY, GANYMEDE, PHOBOS
}
