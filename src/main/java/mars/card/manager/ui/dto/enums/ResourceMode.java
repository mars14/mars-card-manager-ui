package mars.card.manager.ui.dto.enums;

public enum ResourceMode {
    ACTUAL_PLAYER, OTHER_PLAYER, ON_THIS, ON_OTHER, ON_MARS, AFTER_BUY
}
