package mars.card.manager.ui.dto;

import mars.card.manager.ui.dto.enums.ResourceMode;
import mars.card.manager.ui.dto.enums.ResourceType;
import mars.card.manager.ui.dto.enums.Symbol;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class Resource {

    private Symbol symbol;
    private int value;
    private ResourceType resourceType;
    private List<ResourceMode> resourceModes;
    private AreaRequirements areaRequirements;
    private Symbol alternateSymbol;
}
