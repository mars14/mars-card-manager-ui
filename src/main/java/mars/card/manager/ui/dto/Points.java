package mars.card.manager.ui.dto;

import mars.card.manager.ui.dto.enums.Symbol;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Points {

    private int multiplier;
    private int divider;
    private Symbol symbol;
}
