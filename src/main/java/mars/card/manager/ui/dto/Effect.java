package mars.card.manager.ui.dto;

import mars.card.manager.ui.dto.enums.EffectType;
import mars.card.manager.ui.dto.enums.EventType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class Effect {

    private EffectType effectType;
    private List<EventType> eventTypes;
    private List<Resource> benefitResources;
    private List<Resource> requiredResources;
    private boolean optional;
}
