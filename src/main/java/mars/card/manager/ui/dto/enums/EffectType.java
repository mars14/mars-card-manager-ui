package mars.card.manager.ui.dto.enums;

public enum EffectType {
    IMMEDIATE, ACTION, PERMANENT
}
