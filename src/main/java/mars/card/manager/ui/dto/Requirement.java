package mars.card.manager.ui.dto;

import mars.card.manager.ui.dto.enums.LimitType;
import mars.card.manager.ui.dto.enums.Symbol;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Requirement {

    private Symbol symbol;
    private int value;
    private LimitType limitType;
}
