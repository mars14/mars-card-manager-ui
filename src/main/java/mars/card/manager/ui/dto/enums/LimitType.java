package mars.card.manager.ui.dto.enums;

public enum LimitType {
    MIN, MAX
}
