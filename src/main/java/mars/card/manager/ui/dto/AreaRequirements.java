package mars.card.manager.ui.dto;

import mars.card.manager.ui.dto.enums.FieldType;
import mars.card.manager.ui.dto.enums.Symbol;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class AreaRequirements {

    private FieldType fieldType;
    private Symbol requiredNeighbor;
    private int neighborQuantity;
    private Symbol forbiddenNeighbor;
    private List<Symbol> requiredBonuses;
}
