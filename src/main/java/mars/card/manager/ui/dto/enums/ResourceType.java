package mars.card.manager.ui.dto.enums;

public enum ResourceType {
    UNIT, PRODUCTION, CATEGORY, BONUS, EXCHANGE_FACTOR, EXCHANGE_VALUE
}
