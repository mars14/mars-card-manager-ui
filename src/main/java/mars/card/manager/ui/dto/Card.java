package mars.card.manager.ui.dto;

import mars.card.manager.ui.dto.enums.CardType;
import mars.card.manager.ui.dto.enums.Edition;
import mars.card.manager.ui.dto.enums.Symbol;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class Card {

    private String title;
    private CardType cardType;
    private Edition edition;
    private int cost;
    private List<Symbol> categories;
    private List<Requirement> requirements;
    private List<Effect> effects;
    private Points points;
}
