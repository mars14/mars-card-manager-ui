package mars.card.manager.ui.util;

import mars.card.manager.ui.dto.enums.Symbol;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class EnumUtils {

    public static boolean isArea(Symbol value) {
        AtomicBoolean area = new AtomicBoolean(false);
        Optional.ofNullable(value)
                .ifPresent(v -> setArea(area, v));
        return area.get();
    }

    private static void setArea(AtomicBoolean area, Symbol value) {
        area.set(Arrays.stream(Symbol.values())
                .filter(symbol -> symbol.toString().contains("_AREA"))
                .anyMatch(value::equals));
    }
}