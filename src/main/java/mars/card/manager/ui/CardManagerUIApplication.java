package mars.card.manager.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardManagerUIApplication {

    public static void main(String[] args) {
        SpringApplication.run(CardManagerUIApplication.class, args);
    }

}
