package mars.card.manager.ui.view;

import mars.card.manager.ui.dto.AreaRequirements;
import mars.card.manager.ui.dto.enums.FieldType;
import mars.card.manager.ui.dto.enums.Symbol;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.data.binder.Binder;

import static java.util.Objects.isNull;

public class AreaRequirementsComponent extends CustomField<AreaRequirements> {

    private final static String MUST_BE_SET = "Must be set";

    private AreaRequirements areaRequirements;
    private Binder<AreaRequirements> binder;

    private ComboBox<FieldType> fieldType;
    private ComboBox<Symbol> requiredNeighbor;
    private IntegerField neighborQuantity;
    private ListComponent<ComboBox<Symbol>, Symbol> requiredBonuses;
    private ComboBox<Symbol> forbiddenNeighbor;

    public AreaRequirementsComponent() {
        initLayout();

        createBinder();
        addActions();
    }

    @Override
    public void clear() {
        getChildren().findFirst().get().getChildren()
                .forEach(component -> ((HasValue<?, ?>) component).clear());
    }

    private void initLayout() {
        this.fieldType = new ComboBox<>("Field type", FieldType.values());
        fieldType.setPlaceholder("Select...");
        fieldType.setClearButtonVisible(true);

        this.requiredNeighbor = new ComboBox<>("Required neighbor", Symbol.values());
        requiredNeighbor.setPlaceholder("Select...");
        requiredNeighbor.setClearButtonVisible(true);

        this.neighborQuantity = new IntegerField("Neighbor quantity");
        neighborQuantity.setMin(1);
        neighborQuantity.setMax(6);
        neighborQuantity.setHasControls(true);
        neighborQuantity.setVisible(false);

        this.forbiddenNeighbor = new ComboBox<>("Forbidden neighbor", Symbol.values());
        forbiddenNeighbor.setPlaceholder("Select...");
        forbiddenNeighbor.setClearButtonVisible(true);

        this.requiredBonuses = new ListComponent<>("Required bonuses", this::createRequiredBonusesLayout);

        add(new HorizontalLayout(fieldType, requiredNeighbor, neighborQuantity, forbiddenNeighbor, requiredBonuses));
    }

    private void createBinder() {
        this.binder = new Binder<>();
        binder.forField(fieldType)
                .asRequired(MUST_BE_SET)
                .bind(AreaRequirements::getFieldType, AreaRequirements::setFieldType);
        binder.bind(requiredNeighbor, AreaRequirements::getRequiredNeighbor, AreaRequirements::setRequiredNeighbor);
        binder.bind(forbiddenNeighbor, AreaRequirements::getForbiddenNeighbor, AreaRequirements::setForbiddenNeighbor);
        binder.bind(requiredBonuses, AreaRequirements::getRequiredBonuses, AreaRequirements::setRequiredBonuses);
    }

    private void addActions() {
        requiredNeighbor.addValueChangeListener(event ->
                setVisibleNeighborQuantity(!event.getHasValue().isEmpty()));
    }

    private void setVisibleNeighborQuantity(boolean visible) {
        if (visible) {
            neighborQuantity.setVisible(true);
            neighborQuantity.setValue(1);

            binder.forField(neighborQuantity)
                    .asRequired(MUST_BE_SET)
                    .bind(AreaRequirements::getNeighborQuantity, AreaRequirements::setNeighborQuantity);
        } else {
            neighborQuantity.setVisible(false);
            neighborQuantity.clear();
            updateValue();

            binder.removeBinding(neighborQuantity);
        }
    }

    private ComboBox<Symbol> createRequiredBonusesLayout() {
        ComboBox<Symbol> requiredBonusesComboBox = new ComboBox<>(null, Symbol.values());
        requiredBonusesComboBox.setPlaceholder("Select...");
        requiredBonusesComboBox.setClearButtonVisible(true);

        return requiredBonusesComboBox;
    }

    @Override
    protected AreaRequirements generateModelValue() {
        return getValue();
    }

    @Override
    public AreaRequirements getValue() {
        if (isNull(areaRequirements)) {
            areaRequirements = new AreaRequirements();
        }
        if (!binder.writeBeanIfValid(areaRequirements)) {
            areaRequirements = null;
        }
        return areaRequirements;
    }

    @Override
    protected void setPresentationValue(AreaRequirements newPresentationValue) {
        this.areaRequirements = newPresentationValue;
    }
}
