package mars.card.manager.ui.view;

import mars.card.manager.ui.dto.Card;
import mars.card.manager.ui.dto.Effect;
import mars.card.manager.ui.dto.Requirement;
import mars.card.manager.ui.dto.enums.CardType;
import mars.card.manager.ui.dto.enums.Edition;
import mars.card.manager.ui.dto.enums.Symbol;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import kong.unirest.Unirest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

import static mars.card.manager.ui.dto.enums.CardType.CORPORATION;

@Route("main-test")
@UIScope
@SpringComponent
@Slf4j
public class MainView extends VerticalLayout {

    private final static String MUST_BE_SET = "Must be set";

    private Card card;

    @Autowired
    public MainView() {
        this.card = new Card();
    }

    @PostConstruct
    private void initLayout() {
        TextField title = new TextField("Title");
        title.setClearButtonVisible(true);

        ComboBox<Edition> edition = new ComboBox<>("Edition", Edition.values());
        edition.setClearButtonVisible(true);

        ComboBox<CardType> cardType = new ComboBox<>("Card type", CardType.values());
        cardType.setClearButtonVisible(true);

        IntegerField cost = new IntegerField("Cost");
        cost.setValue(0);
        cost.setMin(0);
        cost.setHasControls(true);

        HorizontalLayout infoComponent = new HorizontalLayout(title, edition, cardType, cost);
        infoComponent.setMargin(true);

        ListComponent<ComboBox<Symbol>, Symbol> categories =
                new ListComponent<>("Category", this::createCategoryComponent);

        ListComponent<RequirementComponent, Requirement> requirements =
                new ListComponent<>("Requirement", RequirementComponent::new);

        ListComponent<EffectComponent, Effect> effects =
                new AccordionListComponent<>("Effect", EffectComponent::new);

        PointsComponent points = new PointsComponent();

        Button saveButton = new Button("Save");

        Accordion accordion = new Accordion();
        accordion.add("Info", infoComponent);
        accordion.add("Categories", new VerticalLayout(categories));
        accordion.add("Requirements", new VerticalLayout(requirements));
        accordion.add("Effects", effects);
        accordion.add("Points", new VerticalLayout(points));
        add(accordion, saveButton);

        Binder<Card> binder = new Binder<>();
        binder.forField(title)
                .asRequired(MUST_BE_SET)
                .bind(Card::getTitle, Card::setTitle);
        binder.forField(edition)
                .asRequired(MUST_BE_SET)
                .bind(Card::getEdition, Card::setEdition);
        binder.forField(cardType)
                .asRequired(MUST_BE_SET)
                .bind(Card::getCardType, Card::setCardType);
        binder.bind(cost, Card::getCost, Card::setCost);
        binder.bind(categories, Card::getCategories, Card::setCategories);
        binder.bind(requirements, Card::getRequirements, Card::setRequirements);
        binder.bind(effects, Card::getEffects, Card::setEffects);
        binder.bind(points, Card::getPoints, Card::setPoints);

        cardType.addValueChangeListener(event -> {
            if (CORPORATION.equals(cardType.getValue())) {
                cost.setValue(0);
                cost.setVisible(false);
            } else {
                cost.setVisible(true);
            }
        });

        saveButton.addClickListener(event -> {
            if (binder.writeBeanIfValid(card)) {
                Unirest.post("http://localhost:8081/cards")
                        .header("Content-Type", "application/json")
                        .body(card)
                        .asJson();
                log.info("Card saved: {}", card);
            } else {
                log.warn("Card not saved");
            }
        });
    }

    private ComboBox<Symbol> createCategoryComponent() {
        ComboBox<Symbol> symbolComboBox = new ComboBox<>(null, Symbol.values());
        symbolComboBox.setPlaceholder("Select...");
        symbolComboBox.setClearButtonVisible(true);

        return symbolComboBox;
    }
}
