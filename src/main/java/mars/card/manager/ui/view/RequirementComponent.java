package mars.card.manager.ui.view;

import mars.card.manager.ui.dto.Requirement;
import mars.card.manager.ui.dto.enums.LimitType;
import mars.card.manager.ui.dto.enums.Symbol;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.data.binder.Binder;

import static java.util.Objects.isNull;

public class RequirementComponent extends CustomField<Requirement> {

    private final static String MUST_BE_SET = "Must be set";

    private Requirement requirement;
    private Binder<Requirement> binder;

    private ComboBox<Symbol> symbol;
    private IntegerField value;
    private ComboBox<LimitType> limitType;

    public RequirementComponent() {
        initContent();

        createBinder();
    }

    private void initContent() {
        this.symbol = new ComboBox<>(null, Symbol.values());
        symbol.setPlaceholder("Select...");
        symbol.setClearButtonVisible(true);

        this.value = new IntegerField();
        value.setValue(1);
        value.setMin(1);
        value.setHasControls(true);

        this.limitType = new ComboBox<>(null, LimitType.values());
        limitType.setValue(LimitType.MIN);

        add(new HorizontalLayout(symbol, value, limitType));
    }

    private Binder<Requirement> createBinder() {
        this.binder = new Binder<>();
        binder.forField(symbol)
                .asRequired(MUST_BE_SET)
                .bind(Requirement::getSymbol, Requirement::setSymbol);
        binder.bind(value, Requirement::getValue, Requirement::setValue);
        binder.bind(limitType, Requirement::getLimitType, Requirement::setLimitType);

        return binder;
    }

    @Override
    protected Requirement generateModelValue() {
        return getValue();
    }

    @Override
    public Requirement getValue() {
        if (isNull(requirement)) {
            requirement = new Requirement();
        }
        if (!binder.writeBeanIfValid(requirement)) {
            requirement = null;
        }
        return requirement;
    }

    @Override
    protected void setPresentationValue(Requirement newPresentationValue) {
        this.requirement = newPresentationValue;
    }
}
