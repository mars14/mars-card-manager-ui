package mars.card.manager.ui.view;

import mars.card.manager.ui.dto.Points;
import mars.card.manager.ui.dto.enums.Symbol;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.function.SerializablePredicate;

import static java.util.Objects.isNull;

public class PointsComponent extends CustomField<Points> {

    private final static String MUST_BE_SET = "Must be set";

    private Points points;
    private Binder<Points> binder;

    private IntegerField multiplier;
    private IntegerField divider;
    private ComboBox<Symbol> symbol;

    public PointsComponent() {
        initLayout();

        createBinder();
    }

    private void initLayout() {
        this.multiplier = new IntegerField("Multiplier");
        multiplier.setValue(0);
        multiplier.setHasControls(true);

        this.divider = new IntegerField("Divider");
        divider.setValue(1);
        divider.setMin(1);
        divider.setHasControls(true);

        this.symbol = new ComboBox<>("Symbol", Symbol.values());
        symbol.setPlaceholder("Select...");
        symbol.setClearButtonVisible(true);

        add(new HorizontalLayout(multiplier, divider, symbol));
    }

    private Binder<Points> createBinder() {
        this.binder = new Binder<>();
        binder.forField(multiplier)
                .withValidator(getMultiplierPredicate(), MUST_BE_SET)
                .bind(Points::getMultiplier, Points::setMultiplier);
        binder.bind(divider, Points::getDivider, Points::setDivider);
        binder.forField(symbol)
                .withValidator(getSymbolPredicate(divider), MUST_BE_SET)
                .bind(Points::getSymbol, Points::setSymbol);

        return binder;
    }

    private SerializablePredicate<Integer> getMultiplierPredicate() {
        return value -> value != 0;
    }

    private SerializablePredicate<Symbol> getSymbolPredicate(IntegerField integerField) {
        return value -> !(integerField.getValue() > 1 && isNull(value));
    }

    @Override
    protected Points generateModelValue() {
        return getValue();
    }

    @Override
    public Points getValue() {
        if (isNull(points)) {
            points = new Points();
        }
        if (!binder.writeBeanIfValid(points)) {
            points = null;
        }
        return points;
    }

    @Override
    protected void setPresentationValue(Points newPresentationValue) {
        this.points = newPresentationValue;
    }
}
