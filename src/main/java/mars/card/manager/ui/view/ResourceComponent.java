package mars.card.manager.ui.view;

import mars.card.manager.ui.dto.Resource;
import mars.card.manager.ui.dto.enums.ResourceMode;
import mars.card.manager.ui.dto.enums.ResourceType;
import mars.card.manager.ui.dto.enums.Symbol;
import mars.card.manager.ui.util.EnumUtils;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.function.SerializablePredicate;

import static java.util.Objects.isNull;

public class ResourceComponent extends CustomField<Resource> {

    private final static String MUST_BE_SET = "Must be set";

    private Resource resource;
    private Binder<Resource> binder;

    private ComboBox<Symbol> symbol;
    private ComboBox<Symbol> alternateSymbol;
    private AreaRequirementsComponent areaRequirements;
    private IntegerField value;
    private ComboBox<ResourceType> resourceType;
    private ListComponent<ComboBox<ResourceMode>, ResourceMode> resourceModes;

    public ResourceComponent() {
        initLayout();

        createBinder();
        addActions();
    }

    private void initLayout() {
        this.symbol = new ComboBox<>("Symbol", Symbol.values());
        symbol.setPlaceholder("Select...");
        symbol.setClearButtonVisible(true);

        this.alternateSymbol = new ComboBox<>("Alternate symbol", Symbol.values());
        alternateSymbol.setPlaceholder("Select...");
        alternateSymbol.setClearButtonVisible(true);

        this.areaRequirements = new AreaRequirementsComponent();
        areaRequirements.setVisible(false);

        this.value = new IntegerField("Value");
        value.setValue(1);
        value.setHasControls(true);

        this.resourceType = new ComboBox<>("Resource type", ResourceType.values());
        resourceType.setPlaceholder("Select...");
        resourceType.setClearButtonVisible(true);

        this.resourceModes = new ListComponent<>("Resource mode", this::createResourceModeLayout);

        HorizontalLayout customLayout = new HorizontalLayout(
                symbol, value, resourceType, resourceModes, alternateSymbol);

        add(new VerticalLayout(customLayout, areaRequirements));
    }

    private void createBinder() {
        this.binder = new Binder<>();
        binder.forField(symbol)
                .asRequired(MUST_BE_SET)
                .bind(Resource::getSymbol, Resource::setSymbol);
        binder.bind(alternateSymbol, Resource::getAlternateSymbol, Resource::setAlternateSymbol);
        binder.forField(value)
                .withValidator(getValuePredicate(), MUST_BE_SET)
                .bind(Resource::getValue, Resource::setValue);
        binder.bind(resourceType, Resource::getResourceType, Resource::setResourceType);
        binder.bind(resourceModes, Resource::getResourceModes, Resource::setResourceModes);
    }

    private SerializablePredicate<Integer> getValuePredicate() {
        return value -> value != 0;
    }

    private void addActions() {
        symbol.addValueChangeListener(event -> {
            setVisibleAreaRequirements(EnumUtils.isArea(event.getValue()));
            setVisibleAlternateSymbol(!EnumUtils.isArea(event.getValue()));
        });
    }

    private void setVisibleAreaRequirements(boolean visible) {
        if (visible) {
            areaRequirements.setVisible(true);

            binder.forField(areaRequirements)
                    .asRequired(MUST_BE_SET)
                    .bind(Resource::getAreaRequirements, Resource::setAreaRequirements);
        } else {
            areaRequirements.setVisible(false);
            areaRequirements.clear();
            updateValue();

            binder.removeBinding(areaRequirements);
        }
    }

    private void setVisibleAlternateSymbol(boolean visible) {
        if (visible) {
            alternateSymbol.setVisible(true);

            binder.bind(alternateSymbol, Resource::getAlternateSymbol, Resource::setAlternateSymbol);
        } else {
            alternateSymbol.setVisible(false);
            alternateSymbol.clear();
            updateValue();

            binder.removeBinding(alternateSymbol);
        }
    }

    private ComboBox<ResourceMode> createResourceModeLayout() {
        ComboBox<ResourceMode> resourceModeComboBox = new ComboBox<>(null, ResourceMode.values());
        resourceModeComboBox.setPlaceholder("Select...");
        resourceModeComboBox.setClearButtonVisible(true);

        return resourceModeComboBox;
    }

    @Override
    protected Resource generateModelValue() {
        return getValue();
    }

    @Override
    public Resource getValue() {
        if (isNull(resource)) {
            resource = new Resource();
        }
        if (!binder.writeBeanIfValid(resource)) {
            resource = null;
        }
        return resource;
    }

    @Override
    protected void setPresentationValue(Resource newPresentationValue) {
        this.resource = newPresentationValue;
    }
}
