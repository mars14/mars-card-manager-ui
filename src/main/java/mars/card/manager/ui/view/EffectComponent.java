package mars.card.manager.ui.view;

import mars.card.manager.ui.dto.Effect;
import mars.card.manager.ui.dto.Resource;
import mars.card.manager.ui.dto.enums.EffectType;
import mars.card.manager.ui.dto.enums.EventType;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;

import static mars.card.manager.ui.dto.enums.EffectType.IMMEDIATE;
import static mars.card.manager.ui.dto.enums.EffectType.PERMANENT;
import static java.util.Objects.isNull;

public class EffectComponent extends CustomField<Effect> {

    private final static String MUST_BE_SET = "Must be set";

    private Effect effect;
    private Binder<Effect> binder;

    private ComboBox<EffectType> effectType;
    private Checkbox optional;
    private ListComponent<ComboBox<EventType>, EventType> eventTypes;
    private ListComponent<ResourceComponent, Resource> benefitResources;
    private ListComponent<ResourceComponent, Resource> requiredResources;

    public EffectComponent() {
        initContent();

        createBinder();
        addActions();
    }

    private void initContent() {
        this.effectType = new ComboBox<>("Effect type", EffectType.values());
        effectType.setPlaceholder("Select...");
        effectType.setClearButtonVisible(true);

        this.optional = new Checkbox("optional");

        this.eventTypes = new ListComponent<>("Event type", this::createEventTypeComponent);
        eventTypes.setVisible(false);

        this.benefitResources = new AccordionListComponent<>("Benefit resources", ResourceComponent::new);

        this.requiredResources = new AccordionListComponent<>("Required resources", ResourceComponent::new);

        VerticalLayout layout = new VerticalLayout(effectType, optional);
        layout.setPadding(false);

        add(new VerticalLayout(new HorizontalLayout(layout, eventTypes), benefitResources, requiredResources));
    }

    private void createBinder() {
        this.binder = new Binder<>();
        binder.forField(effectType)
                .asRequired(MUST_BE_SET)
                .bind(Effect::getEffectType, Effect::setEffectType);
        binder.bind(optional, Effect::isOptional, Effect::setOptional);
        binder.forField(benefitResources)
                .asRequired(MUST_BE_SET)
                .bind(Effect::getBenefitResources, Effect::setBenefitResources);
        binder.bind(requiredResources, Effect::getRequiredResources, Effect::setRequiredResources);
    }

    private void addActions() {
        effectType.addValueChangeListener(event ->
                setVisibleEventTypes(event, !IMMEDIATE.equals(event.getValue())));
    }

    private void setVisibleEventTypes(ValueChangeEvent<EffectType> event, boolean visible) {
        if (visible) {
            eventTypes.setVisible(true);
            if (PERMANENT.equals(event.getValue())) {
                binder.forField(eventTypes)
                        .asRequired(MUST_BE_SET)
                        .bind(Effect::getEventTypes, Effect::setEventTypes);
            } else {
                binder.bind(eventTypes, Effect::getEventTypes, Effect::setEventTypes);
            }
        } else {
            eventTypes.setVisible(false);
            eventTypes.clear();
            updateValue();

            binder.removeBinding(eventTypes);
        }
    }

    private ComboBox<EventType> createEventTypeComponent() {
        ComboBox<EventType> eventTypeComboBox = new ComboBox<>(null, EventType.values());
        eventTypeComboBox.setPlaceholder("Select...");
        eventTypeComboBox.setClearButtonVisible(true);

        return eventTypeComboBox;
    }

    @Override
    protected Effect generateModelValue() {
        return getValue();
    }

    @Override
    public Effect getValue() {
        if (isNull(effect)) {
            effect = new Effect();
        }
        if (!binder.writeBeanIfValid(effect)) {
            effect = null;
        }
        return effect;
    }

    @Override
    protected void setPresentationValue(Effect newPresentationValue) {
        this.effect = newPresentationValue;
    }
}
