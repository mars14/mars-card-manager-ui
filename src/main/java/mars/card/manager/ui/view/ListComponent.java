package mars.card.manager.ui.view;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

@Slf4j
public class ListComponent<C extends AbstractField<?, V>, V> extends CustomField<List<V>> {

    private final static int MAX_DEFAULT = 3;

    protected final VerticalLayout layout;
    private final Button addButton;

    private int maxSize;
    private List<V> values;

    public ListComponent(Supplier<C> componentSupplier) {
        this.layout = new VerticalLayout();
        this.addButton = new Button(new Icon(VaadinIcon.PLUS), e -> addRow(componentSupplier));
        this.values = new ArrayList<>();

        this.maxSize = MAX_DEFAULT;

        initContent(componentSupplier);
    }

    public ListComponent(String title, Supplier<C> componentSupplier) {
        this(componentSupplier);
        setTitle(title);
        setPadding(false);
    }

    public ListComponent(Supplier<C> componentSupplier, int maxSize) {
        this(componentSupplier);
        this.maxSize = maxSize;
    }

    public void setTitle(String title) {
        setLabel(title);
    }

    @Override
    public void clear() {
        values.clear();

        layout.getChildren()
                .filter(component -> component != getFirstRow())
                .forEach(component -> removeRow((HorizontalLayout) component));

        ((C) getFirstRow().getComponentAt(0)).clear();
    }

    private void initContent(Supplier<C> componentSupplier) {
        addRow(componentSupplier);
        add(layout);
    }

    protected void addRow(Supplier<C> componentSupplier) {
        HorizontalLayout row = new HorizontalLayout();

        Button deleteButton = new Button(new Icon(VaadinIcon.MINUS), e -> removeRow(row));

        C component = componentSupplier.get();
        component.addValueChangeListener(this::replaceValue);
        component.addDetachListener(this::removeValue);

        row.add(component, addButton, deleteButton);
        layout.add(row);

        performAddActions();
    }

    protected void removeRow(HorizontalLayout row) {
        layout.remove(row);

        performRemoveActions();
    }

    private void performAddActions() {
        if (getRowCount() == 1) {
            setVisibleComponent(getFirstDeleteButton(), false);
        }

        if (getRowCount() == 2) {
            setVisibleComponent(getFirstDeleteButton(), true);
        }

        if (getRowCount() == maxSize) {
            setVisibleComponent(addButton, false);
        }
    }

    private void performRemoveActions() {
        pickUpAddButton();

        if (getRowCount() == 1) {
            setVisibleComponent(getFirstDeleteButton(), false);
        }

        if (getRowCount() == maxSize - 1) {
            setVisibleComponent(addButton, true);
        }
    }

    private void pickUpAddButton() {
        getLastRow().addComponentAtIndex(getLastRow().getComponentCount() - 1, addButton);
    }

    private void setVisibleComponent(Component component, boolean visible) {
        component.setVisible(visible);
    }

    private Button getFirstDeleteButton() {
        return (Button) getFirstRow().getComponentAt(getFirstRow().getComponentCount() - 1);
    }

    protected HorizontalLayout getFirstRow() {
        return (HorizontalLayout) layout.getComponentAt(0);
    }

    protected HorizontalLayout getLastRow() {
        return (HorizontalLayout) layout.getComponentAt(getRowCount() - 1);
    }

    private int getRowCount() {
        return layout.getComponentCount();
    }

    private void replaceValue(ComponentValueChangeEvent<?, V> event) {
        removeValue(event.getOldValue());

        if (!event.getHasValue().isEmpty()) {
            values.add(event.getValue());
        }
    }

    private void removeValue(DetachEvent event) {
        C component = (C) event.getSource();
        removeValue(component.getValue());
    }

    private void removeValue(V value) {
        values.remove(value);
    }

    private void setPadding(boolean padding) {
        layout.setPadding(padding);
    }

    @Override
    protected List<V> generateModelValue() {
        return getValue();
    }

    @Override
    public List<V> getValue() {
        if (values.isEmpty()) {
            return null;
        } else {
            return values;
        }
    }

    @Override
    protected void setPresentationValue(List<V> newPresentationValue) {
        this.values = newPresentationValue;
    }
}