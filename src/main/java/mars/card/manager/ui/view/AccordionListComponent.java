package mars.card.manager.ui.view;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.accordion.AccordionPanel;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import java.util.Objects;
import java.util.function.Supplier;

public class AccordionListComponent<C extends AbstractField<?, V>, V> extends ListComponent<C, V> {

    private String title;

    public AccordionListComponent(String title, Supplier<C> componentSupplier) {
        super(componentSupplier);

        setTitle(title);
    }

    public AccordionListComponent(String title, Supplier<C> componentSupplier, int maxSize) {
        super(componentSupplier, maxSize);

        setTitle(title);
    }

    @Override
    public void setTitle(String title) {
        this.title = title;

        super.layout.getChildren()
                .map(c -> (HorizontalLayout) c)
                .map(this::getPanel)
                .forEach(panel -> panel.setSummaryText(title));
    }

    @Override
    protected void addRow(Supplier<C> componentSupplier) {
        super.addRow(componentSupplier);

        wrapToPanel();
    }

    @Override
    protected void removeRow(HorizontalLayout row) {
        super.removeRow(row);

        if (!getLastPanel().isOpened()) {
            getLastPanel().setOpened(true);
        }
    }

    private void wrapToPanel() {
        HorizontalLayout row = super.getLastRow();
        C component = (C) row.getComponentAt(0);

        AccordionPanel panel = new AccordionPanel(title, component);
        panel.setOpened(true);
        panel.addOpenedChangeListener(e -> {
            if (e.isOpened()) {
                super.layout.getChildren()
                        .map(c -> (HorizontalLayout) c)
                        .map(this::getPanel)
                        .filter(p -> !Objects.equals(p, e.getSource()))
                        .forEach(p -> p.setOpened(false));
            }
        });

        row.addComponentAtIndex(0, panel);
    }

    private AccordionPanel getPanel(HorizontalLayout row) {
        return (AccordionPanel) row.getComponentAt(0);
    }

    private AccordionPanel getLastPanel() {
        return (AccordionPanel) super.getLastRow().getComponentAt(0);
    }
}
